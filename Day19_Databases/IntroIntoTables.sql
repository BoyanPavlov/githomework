CREATE DATABASE task1;
USE task1;

--Product
CREATE TABLE Product(
maker CHAR(4),
model CHAR (1),
typeOfProduct VARCHAR(7)
);

--DROP TABLE Product; --deleting a table

CREATE TABLE Printer(
code INT,
model CHAR(4), 
price DECIMAL(18,2) --is default
);

INSERT INTO Printer(code, model, price)
VALUES(1,'SOGO', 1000.50);

INSERT INTO Printer(code, model, price)
VALUES(2,'BABA', 45.50);

INSERT INTO Printer(code, model, price)
VALUES(3,'AAAA', 75.50),
(4,'OOOO', 5.50);


--adding another column
ALTER TABLE Printer
ADD type VARCHAR(6),
CHECK(type IN ('laser', 'matrix', 'jet'));

--adding a column to table
ALTER TABLE Printer
ADD colour CHAR(6);

ALTER TABLE Printer 
ADD colour CHAR(6) DEFAULT 'n';

--adding codition to table
ALTER TABLE Printer
ADD CONSTRAINT colour
CHECK(type IN ('y', 'n'));

--deleting a column
ALTER TABLE Printer
DROP COLUMN price;

--deleting a table
DROP TABLE Printer;

