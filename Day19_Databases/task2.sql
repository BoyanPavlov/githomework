CREATE DATABASE Flights;
USE Flights;

--deleting a data base -> use another data base, then drop the wrong one and it's deleted
--DROP DATABASE task2;

CREATE TABLE AIRLINE(
code CHAR(2) PRIMARY KEY,
name VARCHAR(50) NOT NULL UNIQUE,
country VARCHAR(50) NOT NULL
);

CREATE TABLE AIRPLANE(
code CHAR(3) PRIMARY KEY,
type VARCHAR(50) NOT NULL,
seats INT NOT NULL CHECK(seats > 0), -- one way to check
year DECIMAL(4,0) NOT NULL,
--CHECK(seats)...
);



CREATE TABLE AIRPORT(
code CHAR(3),
name VARCHAR(50) NOT NULL,
country VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
CONSTRAINT salam UNIQUE (name, country) --UNIQUE element in some scope
);



CREATE TABLE FLIGHT(
fnumber CHAR(6) PRIMARY KEY,
airline_operator CHAR(3) NOT NULL,
dep_airport CHAR(3) NOT NULL,
arr_airport CHAR(3) NOT NULL,
flight_time TIME NOT NULL,
flight_duration INT NOT NULL,
airplane CHAR(3)
);

CREATE TABLE BOOKING(
code CHAR(6),
agency VARCHAR(50) NOT NULL,
airline_code CHAR(2) NOT NULL,
flight_number CHAR(6) NOT NULL,
customer_id INT NOT NULL,
booking_date DATE NOT NULL,
flight_date DATE NOT NULL,
price INT NOT NULL,
status BIT NOT NULL CHECK(status IN (0,1)),
CHECK(flight_date >= booking_date) --second way to check
);

CREATE TABLE CUSTOMER(
id INT PRIMARY KEY,
fname VARCHAR(15) NOT NULL,
uname VARCHAR(15) NOT NULL,
email VARCHAR(30) NOT NULL,
CHECK(email LIKE '%_@%_.%_' AND LEN(email) > 5 )
); 


CREATE TABLE AGENCY(
code VARCHAR(30) NOT NULL,
name VARCHAR(30) NOT NULL,
country VARCHAR(30) NULL,
city VARCHAR(30) NOT NULL
);


INSERT INTO AIRLINE
VALUES
  ('AZ', 'Alitalia', 'Italy'),
  ('BA', 'British Airways', 'United Kingdom'),
  ('LH', 'Lufthansa', 'Germany'),
  ('SR', 'Swissair', 'Switzerland'),
  ('FB', 'Bulgaria Air', 'Bulgaria'),
  ('AF', 'Air France', 'France'),
  ('TK', 'Turkish Airlines', 'Turkey'),
  ('AA', 'American Airlines', 'United States');
 
INSERT INTO AIRPORT
VALUES
  ('SOF', 'Sofia International', 'Bulgaria', 'Sofia'),
  ('CDG', 'Charles De Gaulle', 'France', 'Paris'),
  ('ORY', 'Orly', 'France', 'Paris'),
  ('LBG', 'Le Bourget', 'France', 'Paris'),
  ('JFK', 'John F Kennedy International', 'United States', 'New York'),
  ('ORD', 'Chicago O''Hare International','United States', 'Chicago'),
  ('FCO', 'Leonardo da Vinci International','Italy', 'Rome'),
  ('LHR', 'London Heathrow', 'United Kingdom', 'London');
 
INSERT INTO AIRPLANE
VALUES
  ('319', 'Airbus A319', 150, 1993),
  ('320', 'Airbus A320', 280, 1984),
  ('321', 'Airbus A321', 150, 1989),
  ('100', 'Fokker 100', 80, 1991),
  ('738', 'Boeing 737-800', 90, 1997),
  ('735', 'Boeing 737-800', 90, 1995);
 
INSERT INTO FLIGHT
VALUES
  ('FB1363', 'AZ', 'SOF', 'ORY', '12:45', 100, '738'),
  ('FB1364', 'AZ', 'CDG', 'SOF', '10:00', 120, '321'),
  ('SU2060', 'AZ', 'LBG', 'SOF', '11:10', 110, '738'),
  ('SU2061', 'TK', 'SOF', 'JFK', '13:00', 110, '320'),
  ('FB363', 'FB', 'SOF', 'ORD', '15:10', 110, '738'),
  ('FB364', 'FB', 'LHR', 'SOF', '21:05', 120, '738');
 
INSERT INTO CUSTOMER
VALUES
  (1, 'Petar', 'Milenov', 'petter@mail.com'),
  (2, 'Dimitar', 'Petrov', 'petrov@mail.com'),
  (3, 'Ivan', 'Ivanov', 'ivanov@mail.com'),
  (4, 'Petar', 'Slavov', 'slavov@mail.com'),
  (5, 'Bogdan', 'Bobov', 'bobov@mail.com');

  --TEST
  INSERT INTO CUSTOMER
  VALUES  (6, 'Bogdan', 'Bobov', 'bobovmail.com');

 
INSERT INTO AGENCY
VALUES
  ('Travel One', 'Bulgaria', 'Sofia', '0783482233'),
  ('Travel Two', 'Bulgaria', 'Plovdiv', '02882234'),
  ('Travel Tour', 'Bulgaria', 'Sofia', NULL),
  ('Aerotravel', 'Bulgaria', 'Varna', '02884233');
 
INSERT INTO BOOKING
VALUES
  ('YN298P', 'Travel One', 'FB', 'FB1363', 1, '2013-11-18', '2013-12-25', 300, 0),
  ('YA298P', 'Travel Two', 'FB', 'FB1364', 2, '2013-12-18', '2013-12-25', 300, 1),
  ('YB298P', 'Travel Tour', 'FB', 'SU2060', 3, '2014-01-18', '2014-02-25', 400, 0),
  ('YC298P', 'Travel One', 'FB', 'SU2061', 4, '2014-11-11', '2014-11-25', 350, 0),
  ('YD298P', 'Travel Tour', 'FB', 'FB363', 1, '2013-11-03', '2013-12-20', 250, 1),
  ('YE298P', 'Aerotravel', 'FB', 'FB364', 2, '2013-11-07', '2013-12-21', 150, 0);

  --TEST
  INSERT INTO BOOKING
  VALUES   ('YE298P', 'Aerotravel', 'FB', 'FB364', 2, '2013-12-22', '2013-12-21', 150, 0);