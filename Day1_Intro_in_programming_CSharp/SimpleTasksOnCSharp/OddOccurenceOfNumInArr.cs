﻿
public static class OddOccurenceOfNumInArr
{
    public static int oddOccurenceOfNumber(int[] arr)
    {
        int result = -1;
        int size = arr.Length;
        int count = 0;
        for (int i = 0; i < size; i++)
        {
            count = 1;
            for (int j = i + 1; j < size; j++)
            {
                if (arr[i] == arr[j])
                {
                    count++;
                }
            }
            if (count % 2 == 0 && count != 0)
            {
                result = arr[i];
                break;
            }
        }
        return result;
    }
}


