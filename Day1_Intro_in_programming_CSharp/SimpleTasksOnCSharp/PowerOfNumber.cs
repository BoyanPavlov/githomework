﻿public static class PowerOfNumberClass
{
    public static int PowOfNum(int num, int pow)
    {
        if (pow == 0)
        {
            return 1;
        }
        if (num == 1)
        {
            return num;
        }

        return num * PowOfNum(num, pow-1);
    }
}

