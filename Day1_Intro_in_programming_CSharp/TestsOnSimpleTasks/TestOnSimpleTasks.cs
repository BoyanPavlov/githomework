namespace TestsOnSimpleTasks
{
    [TestClass]
    public class TestOnSimpleTasks
    {

        [TestMethod]
        public void isNumberOdd1()
        {
            int number = 5;
            Assert.AreEqual(false, Odd_Prime_Number.isOdd(number));
        }

        [TestMethod]
        public void isNumberOdd2()
        {
            int number = 6;
            Assert.AreEqual(true, Odd_Prime_Number.isOdd(number));
        }

        [TestMethod]
        public void isNumberPrime()
        {
            int number = 2;
            Assert.AreEqual(true, Odd_Prime_Number.isPrime(number));
        }

        [TestMethod]
        public void isNumberPrime2()
        {
            int number = 6;
            Assert.AreEqual(false, Odd_Prime_Number.isPrime(number));
        }

    }

    [TestClass]
    public class TestsOnMinElementsTasks
    {
        [TestMethod]
        public void findMinElem1()
        {
            int expected = 2;
            int[] arr = { 89, 4, 5, 6, 2, 76 };

            int index = MinElementTasks.findMinIndexOfElement(arr);
            Assert.AreEqual(expected, arr[index]);
        }
        [TestMethod]
        public void TestkthMinElem()
        {
            int expected = 6;
            int[] arr = { 89, 4, 5, 6, 2, 76 };
            int kthElemIndex = 3;

            int number = MinElementTasks.extractKthMinElement(arr, kthElemIndex);
            Assert.AreEqual(expected, number);
        }
    }

    [TestClass]
    public class OddOccurencesOfNumber
    {
        [TestMethod]
        public void TestMethod1()
        {
            int[] arr = { 89, 4, 2, 6, 2, 76 };
            int expected = 2;

            int result = OddOccurenceOfNumInArr.oddOccurenceOfNumber(arr);
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestMethod2()
        {
            int[] arr = { 89, 3, 5, 2, 3, 3, 2 };
            int expected = 2;

            int result = OddOccurenceOfNumInArr.oddOccurenceOfNumber(arr);
            Assert.AreEqual(expected, result);
        }
    }


     [TestClass]
    public class TestPower
    {
        [TestMethod]
        public void TestMethod1()
        {
            int num = 2;
            int pow = 3;
            int expected = 8;
            int result = PowerOfNumberClass.PowOfNum(num,pow);
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestMethod2()
        {
            int num = 2;
            int pow = 4;
            int expected = 16;
            int result = PowerOfNumberClass.PowOfNum(num, pow);
            Assert.AreEqual(expected, result);
        }
    }


}