﻿using Microsoft.AspNetCore.Mvc;
using ToDoTask = ToDoListApp.Entities.ToDoTask;

using ToDoListApp.Entities;

namespace ToDoListApp.Controllers
{
    public class ToDoTaskController : Controller
    {
        public IActionResult Index()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

            using(ToDoListContext context = new ToDoListContext())
            {
                tasks = (from task in context.ToDoTasks
                             join assignment in context.Assignments
                                on task.AssingmentId equals assignment.AssignmentId
                             select new ToDoTask
                             {
                                 Id = task.Id,
                                 Name = task.Name,
                                 Date = task.Date,
                                 Done = task.Done,
                                 Assingment = assignment
                             }).ToList();

            }

            return View(tasks);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult CreateNewTask(ToDoTask task)
        {
            ToDoTask taskToAdd = new ToDoTask();

            using(ToDoListContext context = new ToDoListContext())
            {
                taskToAdd.Name = task.Name;
                taskToAdd.Date = task.Date;
                taskToAdd.Done = false;
                taskToAdd.Assingment = new Assignment
                {
                    AssignedTo = task.Assingment.AssignedTo,
                    CreatedBy = task.Assingment.CreatedBy
                };

                context.ToDoTasks.Add(taskToAdd);
                context.SaveChanges();   
            }

            return RedirectToAction("Index");
        }
    }
}
