﻿namespace ToDoListApp.Models
{
    public class Assignment
    {
        public int AssignmentId { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }

        public virtual List<ToDoTask> Tasks { get; set; }

        public Assignment(int assignmentId, string createdBy, string assignedTo)
        {
            AssignmentId = assignmentId;
            CreatedBy = createdBy;
            AssignedTo = assignedTo;
        }

        public Assignment()
        {
        }
    }
}
