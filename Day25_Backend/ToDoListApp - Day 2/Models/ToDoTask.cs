﻿namespace ToDoListApp.Models
{
    public class ToDoTask
    {
        public int TaskId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool? Done { get; set; }
        public string Description { get; set; }
        public int AssignmentId { get; set; }
        public virtual Assignment Assignment { get; set; }

        public ToDoTask(int taskId, string name, DateTime createdOn, bool? done, string description, int assignmentId)
        {
            TaskId = taskId;
            Name = name;
            CreatedOn = createdOn;
            Done = done;
            Description = description;
            AssignmentId = assignmentId;
        }
        public ToDoTask(int taskId, string name, DateTime createdOn, bool? done, string description, int assignmentId, Assignment assignment)
        {
            TaskId = taskId;
            Name = name;
            CreatedOn = createdOn;
            Done = done;
            Description = description;
            AssignmentId = assignmentId;
            Assignment = assignment;
        }

        public ToDoTask()
        {
        }
    }
}
