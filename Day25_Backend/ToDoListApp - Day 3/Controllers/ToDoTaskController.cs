﻿using Microsoft.AspNetCore.Mvc;
using ToDoTask = ToDoListApp.Entities.ToDoTask;

using ToDoListApp.Entities;
using ToDoListApp.Services;

namespace ToDoListApp.Controllers
{
    public class ToDoTaskController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

           using(ToDoListService toDoListService = new ToDoListService())
           {
                tasks = toDoListService.GetAllTasks();
           }

           if(tasks.Count > 0)
           {
                return View(tasks);
           }

            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ToDoTask task)
        {
            if(task == null)
            {
                return BadRequest();
            }

            ToDoTask taskToAdd = new ToDoTask();
            taskToAdd.Name = task.Name;
            taskToAdd.Date = task.Date;
            taskToAdd.Done = false;
            taskToAdd.Assingment = new Assignment
            {
                AssignedTo = task.Assingment.AssignedTo,
                CreatedBy = task.Assingment.CreatedBy
            };

            using(ToDoListService toDoListService = new ToDoListService())
            {
                toDoListService.CreateToDoTask(taskToAdd);
            }
            
            return RedirectToAction("Index");
        }
    }
}
