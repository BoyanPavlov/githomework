﻿using Microsoft.AspNetCore.Mvc;
using ToDoListApp.Entities;

namespace ToDoListApp.Services
{
    public class ToDoListService : IDisposable
    {
        public void Dispose()
        {
            
        }

        [HttpGet]
        public List<ToDoTask> GetAllTasks()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

            using (ToDoListContext context = new ToDoListContext())
            {
                tasks = (from task in context.ToDoTasks
                         join assignment in context.Assignments
                            on task.AssingmentId equals assignment.AssignmentId
                         select new ToDoTask
                         {
                             Id = task.Id,
                             Name = task.Name,
                             Date = task.Date,
                             Done = task.Done,
                             Assingment = assignment
                         }).ToList();

            }

            return tasks;
        }

        [HttpGet]
        public void CreateToDoTask(ToDoTask taskToAdd)
        {
            using (ToDoListContext context = new ToDoListContext())
            {
                context.ToDoTasks.Add(taskToAdd);
                context.SaveChanges();
            }
        }
    }
}
