﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Xml.Linq;
using ToDoListApp.Models;
using ToDoListApp.Services;


namespace ToDoListApp.Controllers
{

    public class ToDoTaskController : Controller
    {
        private readonly ToDoListService ToDoListServices;

        public ToDoTaskController(ToDoListService toDoListServices)
        {
            ToDoListServices = toDoListServices;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

            tasks = ToDoListServices.GetAllTasks();

            return View(tasks);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var tasks = ToDoListServices.Edit(id);

            return View(tasks);
        }

        [HttpPost]
        public IActionResult Edit(ToDoTask taskToEdit)
        {
            ToDoListServices.Edit(taskToEdit);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            ToDoTask taskDetails = new ToDoTask();

            taskDetails = ToDoListServices.Details(id);

            return View(taskDetails);
        }


        [HttpGet]
        public IActionResult Delete(int id)
        {
            ToDoTask taskDetails = new ToDoTask();

            taskDetails = ToDoListServices.GetTaskById(id);
            ToDoListServices.DeleteTask(taskDetails);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult CreateNewTask(ToDoTask taskToEdit)
        {
            if (taskToEdit == null)
            {
                return BadRequest();
            }

            ToDoTask taskToAdd = new ToDoTask();

            taskToAdd.Name = taskToEdit.Name;
            taskToAdd.Date = taskToEdit.Date;
            taskToAdd.Done = false;
            taskToAdd.Assignment = new Assignment
            {
                AssignedTo = taskToEdit.Assignment.AssignedTo,
                CreatedBy = taskToEdit.Assignment.CreatedBy
            };

            using (ToDoListService toDoListService = new ToDoListService())
            {
                toDoListService.CreateToDoTask(taskToAdd);
            }

            return RedirectToAction("Index");
        }
    }
}
