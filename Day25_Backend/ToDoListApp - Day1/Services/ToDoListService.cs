﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using System.Threading.Tasks;
using ToDoListApp.Models;

namespace ToDoListApp.Services
{
    public class ToDoListService : IDisposable
    {
        public void Dispose()
        {
        }

        [HttpGet]
        public List<ToDoTask> GetAllTasks()
        {
            List<ToDoTask> tasks = new List<ToDoTask>();

            using (ToDoListContext context = new ToDoListContext())
            {
                tasks = (from task in context.ToDoTasks
                         join assignment in context.Assignments
                            on task.AssignmentId equals assignment.AssignmentId
                         select new ToDoTask
                         {
                             Id = task.Id,
                             Name = task.Name,
                             Date = task.Date,
                             Done = task.Done,
                             Assignment = assignment
                         }).ToList();

            }

            return tasks;
        }

        public ToDoTask GetTaskById(int id)
        {
            ToDoTask taskDetails = new ToDoTask();
            using (ToDoListContext context = new ToDoListContext())
            {
                taskDetails = (from task in context.ToDoTasks
                               join assignment in context.Assignments
                                  on task.AssignmentId equals assignment.AssignmentId
                               select new ToDoTask
                               {
                                   Id = task.Id,
                                   Name = task.Name,
                                   Date = task.Date,
                                   Done = task.Done,
                                   Assignment = assignment
                               }).FirstOrDefault(x => x.Id == id);

            }
            return taskDetails;
        }


        [HttpGet]
        public ToDoTask Edit(int id)
        {
            return GetTaskById(id);
        }

        [HttpPost]
        public void Edit(ToDoTask taskToEdit)
        {
            using (ToDoListContext context = new ToDoListContext())
            {
                ToDoTask task = context.ToDoTasks.FirstOrDefault(x => x.Id == taskToEdit.Id);
                Assignment assignment = context.Assignments.FirstOrDefault(x => x.AssignmentId == taskToEdit.Assignment.AssignmentId);

                if (task != null)
                {
                    task.Name = taskToEdit.Name;
                    task.Date = taskToEdit.Date;
                    task.Done = taskToEdit.Done;
                    assignment.AssignedTo = taskToEdit.Assignment.AssignedTo;
                    assignment.CreatedBy = taskToEdit.Assignment.CreatedBy;
                }
                context.SaveChanges();
            }
        }


        [HttpGet]
        public ToDoTask Details(int id)
        {
            return GetTaskById(id);
        }


        [HttpGet]
        public void CreateToDoTask(ToDoTask taskToAdd)
        {
            using (ToDoListContext context = new ToDoListContext())
            {
                context.ToDoTasks.Add(taskToAdd);
                context.SaveChanges();
            }
        }

        [HttpPost]
        public void DeleteTask(ToDoTask taskToRemove)
        {
            using (ToDoListContext context = new ToDoListContext())
            {
                context.Assignments.Remove(taskToRemove.Assignment);
                context.ToDoTasks.Remove(taskToRemove);
                context.SaveChanges();
            }
        }
    }
}
