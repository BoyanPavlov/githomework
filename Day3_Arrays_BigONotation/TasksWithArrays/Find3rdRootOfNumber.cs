﻿using System;

public class Find3rdRoot
{
    // Binary search for a given element in a SORTED array
    // Returns its index if the element exists or -1 otherwise
    static int BinarySearch(int[] array, int x, int size)
    {
        // left and right borders of the searching range
        int left = 0, right = size;

        // while the range is not empty
        while (left < right)
        {
            // find the middle of the range
            int middle = (left + right) / 2;

            if (array[middle] == x) // If the element is found
                return middle;      // return its index

            if (array[middle] < x) // If current element is less than X
                left = middle + 1; // cut the left half
            else
                right = middle; // else cut the right half
        }
        return -1; // If we reach that point the element is not into the array
    }

    static void FillPoweredArray(int[] arr, int size)
    {
        for (int i = 0; i < size; i++)
        {
            arr[i] = (int)Math.Pow(i + 1, 3); // you can use x*x*x instead of Math.Pow. I suspect that the Path.Pow is more complex than the simple implementation
        }
    }

    public static int Find3rdRootOfNumber(int number)
    {
        const int length = 20; // is 20 enough? the largest possible result is 1290
        int[] arr = new int[length]; // do you really need an array with prefilled values? You can use binary search technique without having an array.
        FillPoweredArray(arr, length);
        return (BinarySearch(arr, number, length) + 1);
    }

    static void Main()
    {
    }
}