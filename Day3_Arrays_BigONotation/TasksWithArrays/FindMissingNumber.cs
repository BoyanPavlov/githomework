﻿using System;

public class Program
{
    static void Swap(int[] arr, int pos, int prevPos)
    {
        if (arr[pos] != arr[prevPos]) // for better readablity you can use if (arr[pos] == arr[prevPos]){return;} and avoid nested blocks
        {
            arr[pos] ^= arr[prevPos]; // very unusual approach, but not wrong. using temp will be more readable.
            arr[prevPos] ^= arr[pos]; 
            arr[pos] ^= arr[prevPos];
        }
    }

    static void BubbleSort(int[] array, uint size)
    {
        bool changed;
        uint sorted = 0;
        do
        {
            changed = false;
            for (uint i = size - 1; i > sorted; --i) // should be fine to use int, instead of uint to avod the casting below
            {
                if (array[i] < array[i - 1])
                {
                    Swap(array, (int)i, (int)(i - 1));
                    changed = true;
                }
            }
            ++sorted;
        } while (changed);
    }

    // remove the size parameter. use array.Length internaly. 
    // Possible conflict because of inconsistent input parameters. 
    // ex. ReturnMissingNumber(new int[]{1,3}, 150);
    public static int ReturnMissingNumber(int[] arr, int size) 
    {
        int result = -1;
        BubbleSort(arr, (uint)size);
        for (int i = 1; i < size; i++)
        {
            if (Math.Abs(arr[i - 1] - arr[i]) > 1)
            {
                result = arr[i - 1] + 1; 
                break; // you found the element, use 'return arr[i - 1] + 1;' to exit the method here. no need for a break;
            }
        }
        return result;
    }

    static void Main() // Main not needed here. try to delete it and change the project output type to 'class library' instead of 'console application'.
    {
    }
}