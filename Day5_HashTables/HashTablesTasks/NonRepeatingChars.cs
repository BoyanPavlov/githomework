﻿using System.Linq;

public static class NonRepeatingChars
{

    public static List<char> getSetOfChars(string input)
    {
        Dictionary<char, int> setOfLetters = new();

        List<char> setOfChars = new();
        for (int i = 0; i < input.Length; i++)
        {
            if (setOfLetters.ContainsKey(input[i]))
            {
                setOfLetters[input[i]]++;
            }
            else
            {
                setOfLetters.Add(input[i], 1);
            }
        }

        foreach (var item in setOfLetters)
        {
            if ('A' <= item.Key && item.Key <= 'Z' ||
            'a' <= item.Key && item.Key <= 'z')
            {

                setOfChars.Add(item.Key);
            }
        }

        return setOfChars;
    }
}

