﻿using System;
using System.Collections.Generic;

public static class SpellChecker
{
    public static string extractWordFromString(string sentence, ref int from)
    {
        string word = "";
        if (sentence.Length == 0)
        {
            return word;
        }

        while (from < sentence.Length && (sentence[from] == ' ' || sentence[from] == ',' || sentence[from] == '.' ||
            sentence[from] == '!' || sentence[from] == '?'))
        {
            from++;
        }

        while (from < sentence.Length && !(sentence[from] == ' ' || sentence[from] == ',' || sentence[from] == '.' ||
            sentence[from] == '!' || sentence[from] == '?'))
        {
            word += sentence[from];
            from++;
        }
        return word;
    }

    public static string[] extractWords(string sentence)
    {
        List<string> words = new List<string>();
        int from = 0;
        while (from < sentence.Length)
        {
            words.Add(extractWordFromString(sentence, ref from));
        }

        //or
        //string[] words1 = sentence.Split(new char[] { ' ', ',', '.', '!', '?' },
        //    StringSplitOptions.RemoveEmptyEntries);

        return words.ToArray();
    }

    public static string extractMisspelledOrMissingWords(string sentence, HashSet<string> dic)
    {
        string[] words = extractWords(sentence);
        string missingOrMisspelledWords = "";
        foreach (string word in words)
        {
            if (!dic.Contains(word))
            {
                missingOrMisspelledWords += word + ", ";
            }
        }
        return missingOrMisspelledWords;
    }

    public static List<string> extractMisspelledWords(string input, HashSet<string> words)
    {
        var result = new List<string>();

        return result;
    }
}

