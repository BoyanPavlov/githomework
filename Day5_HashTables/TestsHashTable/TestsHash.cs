namespace TestsHashTable
{

    [TestClass]
    public class Task1
    {
        [TestMethod]
        public void findCoolestPlate()//max letters in a numberplate
        {
            CarPlate.CarPlateToOwner.Add("PB5839TX", "Georgi Georgiev");
            CarPlate.CarPlateToOwner.Add("CB8888CB", "Stefan Gerdjikov");
            CarPlate.CarPlateToOwner.Add("EB5849AH", "Blagovest Evlogiev");
            CarPlate.CarPlateToOwner.Add("CB7B7B7B", "Bojidar Serafimov");

            KeyValuePair<string, string> expected =
                new KeyValuePair<string, string>("CB7B7B7B", "Bojidar Serafimov");
            KeyValuePair<string, string> result = CarPlate.returnCoolestNumberPlate();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void findTheMultiCarOwner()//max letters in a numberplate
        {
            CarPlate.CarPlateToOwner.Add("CA7B7B7B", "Bojidar Serafimov");

            string expected = "Bojidar Serafimov";
            string result = CarPlate.getTheOwnerWithMostCars();
            Assert.AreEqual(expected, result);
        }

    }

    [TestClass]
    public class Task3
    {
        [TestMethod]
        public void TestGivenSetOfChars1()
        {
            string input = "Hell world";
            List<char> set = new List<char> { 'H', 'e', 'l', 'w', 'o', 'r', 'd' };
            List<char> result = NonRepeatingChars.getSetOfChars(input);
            CollectionAssert.AreEquivalent(set, result);
        }

        [TestMethod]
        public void TestGivenSetOfChars2()
        {
            string input = "lllllll";
            List<char> set = new List<char> { 'l' };
            List<char> result = NonRepeatingChars.getSetOfChars(input);
            CollectionAssert.AreEquivalent(set, result);
        }

    }

    [TestClass]
    public class Task2
    {
        [TestMethod]
        public void TestGivenSetOfChars1()
        {
            int[] arr = { 1, 2, 3, 4, 4 };
            int[] arr1 = { 2, 2, 4, 4 };
            int[] expected = { 2, 4 };
            int[] result = IntersectionOfArrays.extractIntersection(arr1, arr);

            CollectionAssert.AreEquivalent(expected, result);
        }

    }

    [TestClass]

    public class TestingSpellCheckerClass
    {
        [TestMethod]
        public void checkExtractingOfAWord()
        {
            string expected = "Hello";
            string sentence = "Hello my dear world";
            int pos = 0;
            string result = SpellChecker.extractWordFromString(sentence, ref pos);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void checkExtractingOfAWord1()
        {
            string expected = "my";
            string sentence = "Hello my dear world";
            int pos = 5;
            string result = SpellChecker.extractWordFromString(sentence, ref pos);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void checkExtractingOfAWord2()
        {
            string expected = "Hello";
            string expected1 = "my";
            string expected2 = "dear";
            string expected3 = "world";
            string sentence = "Hello my dear world";
            int pos = 0;
            string result = SpellChecker.extractWordFromString(sentence, ref pos);
            string result1 = SpellChecker.extractWordFromString(sentence, ref pos);
            string result2 = SpellChecker.extractWordFromString(sentence, ref pos);
            string result3 = SpellChecker.extractWordFromString(sentence, ref pos);
            Assert.AreEqual(expected, result);
            Assert.AreEqual(expected1, result1);
            Assert.AreEqual(expected2, result2);
            Assert.AreEqual(expected3, result3);
        }

        [TestMethod]
        public void extractAllWords()
        {
            string[] words = { "Hello", "my", "dear", "world" };
            string sentence = "Hello, my dear world";
            string[] result = SpellChecker.extractWords(sentence);
            CollectionAssert.AreEqual(words, result);
        }
        [TestMethod]
        public void testMissingOrMisspelledWords()
        {
            string sentence = "Hell, my deer word";
            HashSet<string> words = new HashSet<string>() { "Hello", "my", "dear", "world" };
            string expected = "Hell, deer, word, ";
            string result = SpellChecker.extractMisspelledOrMissingWords(sentence, words);
            Assert.AreEqual(expected, result);
        }
    }

    [TestClass]
    public class TestAnagramsClass
    {
        [TestMethod]
        public void TestAnagrams()
        {
            string[] words = { "listen", "silent", "pot", "top", "part", "trap", "dog", "god" };
            var actual = AnagramTask.returnListsOfAnagrams(words);

            var expected = new List<List<string>>();
            expected.Add(new List<string> { "listen", "silent" });
            expected.Add(new List<string> { "pot", "top" });
            expected.Add(new List<string> { "part", "trap" });
            expected.Add(new List<string> { "dog", "god" });

            CollectionAssert.Equals(expected, actual);
        }
    }
}
