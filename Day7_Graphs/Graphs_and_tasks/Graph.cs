﻿namespace Graphs_and_tasks
{
    public class Graph
    {
        private int m_vertex_count;
        List<List<int>> m_relations;

        public int VertexCount
        {
            get { return m_vertex_count; }
            set { m_vertex_count = value; }
        }

        public List<List<int>> Relations
        {
            get { return m_relations; }
            set { m_relations = value; }
        }

        public Graph(int count)
        {
            m_vertex_count = count;
            m_relations = new List<List<int>>();
            for (int i = 0; i < m_vertex_count; i++)
            {
                m_relations.Add(new List<int>());
                for (int j = 0; j < m_vertex_count; j++)
                {
                    m_relations[i].Add(0);
                }
            }
        }

        public void AddEdge(int start, int end)
        {
            m_relations[start][end] = 1;
            m_relations[end][start] = 1;
        }


        public List<int> BFS(int startingEdge)
        {
            List<int> sequenceOfEdgesInBFS = new List<int>();
            bool[] visited = new bool[m_vertex_count];

            Queue<int> trackVisited_Queue = new Queue<int>();
            visited[startingEdge] = true;

            trackVisited_Queue.Enqueue(startingEdge);
            while (trackVisited_Queue.Count != 0)
            {
                startingEdge = trackVisited_Queue.Dequeue();
                sequenceOfEdgesInBFS.Add(startingEdge);

                for (int i = 0; i < m_vertex_count; i++)
                {
                    if (m_relations[startingEdge][i] == 1 && !visited[i])
                    {
                        visited[i] = true;
                        trackVisited_Queue.Enqueue(i);
                    }
                }
            }

            return sequenceOfEdgesInBFS;
        }

        public void DFS_recursive(int node, bool[] visited, List<int> sequence)
        {
            visited[node] = true;
            sequence.Add(node);
            for (int i = 0; i < m_vertex_count; i++)
            {
                if (!visited[i] && m_relations[node][i] != 0)
                {
                    DFS_recursive(i, visited, sequence);
                }
            }
        }


        private bool doesGraphContainCycleHelper(int node, bool[] visited, int parent)
        {
            visited[node] = true;
            for (int i = 0; i < m_vertex_count; i++)
            {
                if (m_relations[node][i] != 0)
                {
                    if (!visited[i])
                    {
                        if (doesGraphContainCycleHelper(i, visited, node))
                        {
                            return true;
                        }
                    }
                    else if (i != parent)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool doesGraphContainCycle()
        {
            bool[] visited = new bool[m_vertex_count];
            for (int i = 0; i < m_vertex_count; i++)
            {
                if (!visited[i] && doesGraphContainCycleHelper(i, visited, -1))
                {
                    return true;
                }
            }
            return false;
        }

    }
}