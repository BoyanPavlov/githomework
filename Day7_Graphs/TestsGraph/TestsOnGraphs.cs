using Graphs_and_tasks;

namespace TestsGraph
{
    [TestClass]
    public class TestsOnGraphs
    {
        [TestMethod]
        public void TestMyBFS()
        {
            Graph graph = new Graph(6);

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(1, 3);
            graph.AddEdge(2, 4);
            graph.AddEdge(2, 5);

            List<int> bfsSequence = graph.BFS(0);

            List<int> expected = new List<int>() { 0, 1, 2, 3, 4, 5 };
            CollectionAssert.AreEqual(expected, bfsSequence);

        }

        [TestMethod]
        public void TestMyDFS()
        {
            Graph graph = new Graph(6);

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(1, 3);
            graph.AddEdge(2, 4);
            graph.AddEdge(2, 5);

            bool[] visited = new bool[graph.VertexCount];
            List<int> dfsSequence = new List<int>();

            graph.DFS_recursive(0, visited, dfsSequence);


            List<int> expected = new List<int>() { 0, 1, 3, 2, 4, 5 };
            CollectionAssert.AreEqual(expected, dfsSequence);

        }
    }

    [TestClass]
    public class GraphTests
    {
        [TestMethod]
        public void TestGraphWithCycle()
        {
            Graph graph = new Graph(4);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 0);

            Assert.IsTrue(graph.doesGraphContainCycle());
        }

        [TestMethod]
        public void TestGraphWithoutCycle()
        {
            Graph graph = new Graph(4);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 3);

            Assert.IsFalse(graph.doesGraphContainCycle());
        }

        [TestMethod]
        public void TestGraphWithSelfLoop()
        {
            Graph graph = new Graph(4);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 3);

            Assert.IsTrue(graph.doesGraphContainCycle());
        }

        [TestMethod]
        public void TestGraphWithMultipleComponents()
        {
            Graph graph = new Graph(6);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 0);
            graph.AddEdge(3, 4);
            graph.AddEdge(4, 5);

            Assert.IsTrue(graph.doesGraphContainCycle());
        }
    }
}