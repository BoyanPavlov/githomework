﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nemetschek.OOPCSharp.Animal
{
    internal struct Animal
    {
        public int Age;
        public abstract void Move();
    }
}
