﻿namespace OOPFileSystem
{
    public class File
    {
        private string path;
        private string filename;
        private int size;
        private int highestRow;
        private Dictionary<int, string> content;
        public File(string currentPath, string filename)
        {

            this.filename = filename;
            this.path = currentPath + "\\" + filename;
            highestRow = 0;
            size = 1;
            content = new() { };

        }

        public void Write(int row, string contentToAdd)
        {
            if (this.content.ContainsKey(row))
            {
                this.content[row] += contentToAdd;
            }
            else
            {
                this.content.Add(row, contentToAdd);
            }

            if (row > highestRow)
            {
                size -= highestRow;
                size += row;

                highestRow = row;
            }

            size += contentToAdd.Length;
        }

        public string Cat()
        {
            return Tail(0);
        }

        public string Tail(int rows = 10)
        {
            int begining = Math.Max(highestRow - rows, 0);

            string contents = "";

            for (int i = begining; i <= highestRow; i++)
            {
                if (content.ContainsKey(i))
                {
                    contents += content[i];
                }
                contents += "\n";
            }

            return contents;
        }

        public int Wc()
        {
            int words = 0;

            foreach (var entry in content)
            {
                words += Wc(entry.Value);
            }

            return words;
        }

        public static int Wc(string str)
        {
            char[] delimiters = { ' ', ',', '.', '!', '?', '-' };

            string[] words = str.Split(delimiters);

            int wordCount = 0;

            foreach (string word in words)
            {
                if (word.Length > 0)
                {
                    wordCount++;
                }
            }

            return wordCount;

        }
    }
}
