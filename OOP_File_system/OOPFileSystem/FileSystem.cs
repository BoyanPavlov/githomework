﻿using System;

namespace OOPFileSystem
{
    public class FileSystem
    {
        private Folder homeFolder;
        private Folder currentFolder;

        public FileSystem()
        {
            homeFolder = new Folder("", "home");
            currentFolder = homeFolder;
        }

        private bool Cd_helper(string path, Folder currentFolder)
        {
            foreach (var item in currentFolder.Folders)
            {
                if (path == item.Key)
                {
                    currentFolder = item.Value;
                    return true;
                }
                else
                {
                    return true || Cd_helper(path, item.Value);
                }

            }
            return false;
        }

        public void Cd(string path)
        {
            //case current folder, should make it work with .
            string[] parts = path.Split('\\');
            if (parts[0] == "." || (parts.Length == 1))
            {
                if (currentFolder.Folders != null)
                {
                    foreach (var item in currentFolder.Folders)
                    {
                        if (path == item.Key)
                        {
                            currentFolder = item.Value;
                            return;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Path doesn't exist");
                    return;
                }
            }
            //TODO
            //add implementation for .. or for the whole directory


            bool found = Cd_helper(path, currentFolder);



            if (found == false)
            {
                Console.WriteLine("Path doesn't exist, try again\n");
            }
        }


        //I know - we have repetetive code here, but in projects am I allowed to delete common code?
        //The function called HandleInput is working and it belongs to both of us, so I'm not changing it
        //If that's not how the things works - please let me know. Greetings, Boyan

        //currently tested with "pwd | exit"
        //Undefined behaviour - using pipe with commands which doesn't return string (in this program)
        public bool HandleComplexInput(string input)
        {
            string[] commands = input.Split('|');
            string output = "";

            for (int i = 0; i < commands.Length; i++)
            {
                string[] commandArray = commands[i].Split(' ');
                if (i > 0)
                {
                    if (commandArray[0] == "")
                    {
                        commandArray[0] = commandArray[1];
                    }
                    commandArray[1] = output;
                }
                switch (commandArray[0])
                {
                    case "pwd":
                        output = currentFolder.Path;
                        break;
                    case "cd":
                        Cd(commandArray[1]);
                        break;
                    case "mkdir":
                        currentFolder.Mkdir(commandArray[1]);
                        break;
                    case "create_file":
                        currentFolder.CreateFile(commandArray[1]);
                        break;
                    case "cat":
                        output = (currentFolder.Cat(commandArray[1]));
                        break;
                    case "tail":
                        output = currentFolder.Tail(commandArray[1]);
                        break;
                    case "write":
                        int row = int.Parse(commandArray[2]);
                        string textToAdd = string.Join(" ", new ArraySegment<string>(commandArray, 3, commandArray.Length - 3));
                        currentFolder.Write(commandArray[1], row, textToAdd);
                        break;
                    case "ls":
                        output = currentFolder.Ls();
                        break;
                    case "wc":
                        if (commandArray[1] != "-f")
                        {
                            string TextToCheck = string.Join(" ", new ArraySegment<string>(commandArray, 1, commandArray.Length - 1));
                            Console.WriteLine(File.Wc(TextToCheck) + '\n');
                        }
                        else
                        {
                            Console.Write(currentFolder.Wc(commandArray[2]));
                        }
                        break;
                    case "exit":

                        if (output != "")
                        {
                            Console.WriteLine(output);
                        }

                        Console.WriteLine(">\tHave a nice day! =) \n");
                        return false;
                    default:
                        Console.WriteLine(string.Format("Command {1} does not exist", commandArray[0]));
                        break;
                }
            }
            Console.WriteLine(output);
            return true;
        }

        public bool HandleInput(string command)
        {
            if (command.Contains('|'))
            {
                return HandleComplexInput(command);
            }

            string[] commandArray = command.Split(' ');

            switch (commandArray[0])
            {
                case "pwd":
                    Console.WriteLine(currentFolder.Path);
                    break;
                case "cd":
                    Cd(commandArray[1]);
                    break;
                case "mkdir":
                    currentFolder.Mkdir(commandArray[1]);
                    break;
                case "create_file":
                    currentFolder.CreateFile(commandArray[1]);
                    break;
                case "cat":
                    Console.WriteLine(currentFolder.Cat(commandArray[1]));
                    break;
                case "tail":
                    Console.WriteLine(currentFolder.Tail(commandArray[1]));
                    break;
                case "write":
                    int row = int.Parse(commandArray[2]);
                    string textToAdd = string.Join(" ", new ArraySegment<string>(commandArray, 3, commandArray.Length - 3));
                    currentFolder.Write(commandArray[1], row, textToAdd);
                    break;
                case "ls":
                    Console.WriteLine(currentFolder.Ls());
                    break;
                case "wc":
                    if (commandArray[1] != "-f")
                    {
                        string TextToCheck = string.Join(" ", new ArraySegment<string>(commandArray, 1, commandArray.Length - 1));
                        Console.WriteLine(File.Wc(TextToCheck) + '\n');
                    }
                    else
                    {
                        Console.Write(currentFolder.Wc(commandArray[2]));
                    }
                    break;
                case "exit":
                    Console.WriteLine(">\tHave a nice day! =) \n");
                    return false;
                default:
                    Console.WriteLine(string.Format("Command {0} does not exist", commandArray[0]));
                    break;
            }
            return true;
        }
    }
}
