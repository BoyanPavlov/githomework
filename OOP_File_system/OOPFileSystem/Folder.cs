﻿namespace OOPFileSystem
{
    public class Folder
    {
        private string nameOfFolder;
        private string path;

        Dictionary<string, Folder> folders;
        Dictionary<string, File> files;

        public Folder(string currentPath, string name)
        {
            this.path = currentPath + "\\" + name;
            this.nameOfFolder = name;
            folders = new Dictionary<string, Folder>();
            files = new Dictionary<string, File>();
        }

        public string Name
        {
            get { return nameOfFolder; }

            set
            {
                nameOfFolder = value;
                string[] parts = path.Split('\\');
                if (parts.Length > 0)
                {
                    parts[parts.Length - 1] = value;
                    path = string.Join("\\", parts);
                }
            }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public Dictionary<string, Folder> Folders
            => folders;


        public Dictionary<string, File> Files
        {
            get { return files; }
        }

        public string Ls()
        {
            string output = "";
            foreach (var folder in folders)
            {
                output += folder.Key + "\\" + "\t";
            }

            foreach (var file in files)
            {
                output += file.Key + "\t";
            }

            return output;
        }

        //creating a folder is available only in the current dirrectory
        public void Mkdir(string folderName)
        {
            Folder newFolder = new(path, folderName);
            folders.Add(folderName, newFolder);
        }

        public void CreateFile(string filename)
        {
            File newFile = new(path, filename);
            files.Add(filename, newFile);
        }
        public void Write(string filepath, int row, string text)
        {
            List<string> fileapathArr = filepath.Split('\\').ToList();

            if (fileapathArr[0] == Name || fileapathArr[0] == ".")
            {
                fileapathArr.RemoveAt(0);
            }

            if (files.ContainsKey(fileapathArr[0]))
            {
                files[fileapathArr[0]].Write(row, text);
            }
            else if (folders.ContainsKey(fileapathArr[0]))
            {
                string folder = fileapathArr[0];
                fileapathArr.RemoveAt(0);

                string subpath = string.Join('\\', fileapathArr);

                folders[folder].Write(subpath, row, text);
            }
            else
            {
                Console.WriteLine("File does not exist in current directory");
            }
        }

        private File FileFromPath(string filepath)
        {
            List<string> fileapathArr = filepath.Split('\\').ToList();

            if (fileapathArr[0] == Name || fileapathArr[0] == ".")
            {
                fileapathArr.RemoveAt(0);
            }

            if (files.ContainsKey(fileapathArr[0]))
            {
                return files[fileapathArr[0]];
            }
            else if (folders.ContainsKey(fileapathArr[0]))
            {
                string folder = fileapathArr[0];
                fileapathArr.RemoveAt(0);

                string subpath = string.Join('\\', fileapathArr);

                return folders[folder].FileFromPath(subpath);
            }
            else
            {
                return null;
            }
        }
        public string Cat(string filepath)
        {
            File fileToCat = FileFromPath(filepath);

            if (fileToCat == null)
            {
                return "File does not exist in current directory";
            }

            return fileToCat.Cat();
        }

        public string Tail(string filepath)
        {
            File fileToTail = FileFromPath(filepath);

            if (fileToTail == null)
            {
                return ("File does not exist in current directory");
            }

            return fileToTail.Tail();
        }

        public int Wc(string filepath)
        {
            File fileToWc = FileFromPath(filepath);

            if (fileToWc == null)
            {
                return -1;
            }

            return fileToWc.Wc();
        }
    }
}
