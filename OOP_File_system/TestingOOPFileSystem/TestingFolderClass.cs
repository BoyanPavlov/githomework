using OOPFileSystem;

namespace TestingOOPFileSystem
{
    [TestClass]
    public class TestingFolderClass
    {
        [TestMethod]
        public void TestRenamingAFolder()
        {
            Folder myFolder = new Folder("Hello\\Today", "Test");
            string expectedPath = "Hello\\Today\\Test1";
            myFolder.Name= "Test1";
            Assert.AreEqual(expectedPath, myFolder.Path);
        }

    }
}