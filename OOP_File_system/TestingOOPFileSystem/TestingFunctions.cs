﻿using OOPFileSystem;

namespace TestingOOPFileSystem
{
    [TestClass]
    public class TestingFunctions
    {
        [TestMethod]
        public void TestingLsAndMkdir()
        {
            Folder home = new("\\home", "folder1");
            home.Mkdir("folder2");
            home.Mkdir("folder3");
            string result = home.Ls();
            string expectedLs = "folder2\\" + '\t' + "folder3\\" + '\t';
            Console.WriteLine(result);
            Assert.AreEqual(expectedLs, result);
        }


        [TestMethod]
        public void TestingLsAndCreateFile()
        {
            Folder home = new("\\home", "folder1");
            home.CreateFile("file1.txt");
            home.CreateFile("pic1.png");
            string result = home.Ls();
            string expectedLs = "file1.txt" + '\t' + "pic1.png" + '\t';
            Console.WriteLine(result);
            Assert.AreEqual(expectedLs, result);
        }

    }
}