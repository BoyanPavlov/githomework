﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    public abstract class Animal : Food
    {
        private readonly int _maxEnergy = 10;
        private int _currentEnergy;

        private readonly HashSet<Food> _diet;
        private bool _alive = true;

        private int _lifeSpan;

        private Behavior _animalsBehavior;
        public int LifeSpan
        {
            get; set;
        }

        public HashSet<Food> Diet => _diet;

        public bool isAlive
        {
            get
            { return _alive; }

            set
            {
                _alive = value;
                if (!_alive) { _currentEnergy = 0; }
                else { _currentEnergy = _maxEnergy; }
            }
        }

        public int CurrentEnergy  
            => _currentEnergy;
        public int MaxEnergy
            => _maxEnergy;

        public Animal(HashSet<Food> diet, int nutritionalValue, string name, Behavior behavior)
           : base(nutritionalValue, name)
        {
            _diet = diet;
            _currentEnergy = _maxEnergy;
            _animalsBehavior = behavior;
            _lifeSpan = 0;
        }

        public sealed override bool isAnimal()
        {
            return true;
        }

        public void Feed(Food givenFood)
        {
            if (isAlive)
            {
                if(_diet.Contains(givenFood))
                {
                    _currentEnergy += givenFood.NutritionValue;
                    if (_currentEnergy > _maxEnergy)
                    {
                        _currentEnergy = _maxEnergy;
                    }
                    else
                    {
                        _animalsBehavior.FeedingSound();
                    }
                }
                else
                {
                    if (_currentEnergy > 0)
                    {
                        _currentEnergy--;
                        _animalsBehavior.StarvingSound();

                        int temp = (_maxEnergy / 2 - 1);
                        if (_currentEnergy < temp)
                        {
                            _animalsBehavior.HungrySound();
                        }
                    }
                    else
                    {
                        _currentEnergy = 0;
                        _alive = false;
                        _animalsBehavior.DyingSound();
                    }

                }
            }
            else
            {
                _animalsBehavior.errorMessageForFeeding();
            }
        }

        public void AnimalHasBeenEaten()
        {
            _currentEnergy = 0;
            _alive = false;
            _animalsBehavior.animalHasBeenEatenMessege();
        }

    }
}
