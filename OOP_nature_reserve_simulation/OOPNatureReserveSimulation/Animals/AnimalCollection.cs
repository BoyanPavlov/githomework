﻿using OOPNatureReserveSimulation.Foods;

namespace OOPNatureReserveSimulation.Animals
{
    public static class AnimalCollection
    {
    
        static public HashSet<Animal> varietyOfAnimals = new HashSet<Animal>()
        {
           new Cat(), new Cow(),new Dog(), new SeaEagle(), new Wolf()
        };


        public static void addFoodInCollection(Animal input)
        {
            if (!varietyOfAnimals.Contains(input))
            {
                varietyOfAnimals.Add(input);
            }
        }

        public static Food getRandomFood()
        {
            Random randomizer = new Random();
            Food[] asArray = varietyOfAnimals.ToArray();
            Food randomFoodCollection = asArray[randomizer.Next(asArray.Length)];
            return randomFoodCollection;
        }

    }
}
