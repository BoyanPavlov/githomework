﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    public class Cat : Animal
    {
        public Cat() : base(
            new HashSet<Food>() { new Milk(), new Fish(), new Meat(), new SeaEagle() },
            (int)AnimalsNutrVal.e_cat,
            "Cat",//TODO create enum for Animals name
            new Behavior("Cat", ("Nom, nom!"), ("Ps ps!"), ("Meow Meow!"))
            )
        {
        }
    }
}
//created OOP branch
