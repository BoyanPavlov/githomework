﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    public class Cow : Animal
    {
        public Cow() : base(new HashSet<Food>(){new Blueberries(),new Carrots(),
            new Strawberries(),new Grass() }, (int)AnimalsNutrVal.e_cow,
            "Cow",
            new Behavior("Cow", "Hrup Hrup!", "Phrr !", "Muuu !")
            )
        {
        }
    }
}
