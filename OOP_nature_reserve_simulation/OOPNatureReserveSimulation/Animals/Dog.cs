﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    public class Dog : Animal
    {
        public Dog() : base(new HashSet<Food> { new Bones(), new Meat() }
        , (int)AnimalsNutrVal.e_dog,
            "Dog",
            new Behavior("Dog", "Nom, nom!", "Snif, Snif!", "Bark, bark!"))
        {
        }
    }
}
