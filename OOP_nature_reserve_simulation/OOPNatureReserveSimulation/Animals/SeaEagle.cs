﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    internal class SeaEagle : Animal
    {
        public SeaEagle() : base(
           new HashSet<Food>() { new Blueberries(), new Carrots(),
               new Strawberries(), new Fish(), new Meat() },
           (int)AnimalsNutrVal.e_seaegle,
           "SeaEagle",
            new Behavior("SeaEagle", "Kakao!", "Haa Haa!", "Cruch Cruch!")
            )

        {
        }
    }
}
