﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.UI;

namespace OOPNatureReserveSimulation.Animals
{
    public class Wolf : Animal
    {
        public Wolf() : base(new HashSet<Food> { new Bones(), new Meat(), new Cow(), new SeaEagle() }
        , (int)AnimalsNutrVal.e_wolf,
            "Wolf",
              new Behavior("Wolf", "Nom, nom!", "ahrr!", "Awooo!")
            )
        {
        }
    }
}
