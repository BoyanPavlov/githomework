﻿using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.Animals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPNatureReserveSimulation.Biomes
{
    //abstract class for plain, forest, sea, ocean, mountain
    public abstract class Biome
    {
        private string _name;

        private HashSet<Food> _animalsAndFoodAllowedInCurrentBiome;

        private List<KeyValuePair<int, Food>> _currentAnimalsAndFoodInTheBiome;

        private KeyValuePair<int, int> _position;


        public string Name { get => _name; }
        public List<KeyValuePair<int, Food>> CurrentAnimalsAndFoodInTheBiom
        {
            get => _currentAnimalsAndFoodInTheBiome;
            set => _currentAnimalsAndFoodInTheBiome = value;
        }
        public KeyValuePair<int, int> Position
        {
            get => _position;
        }
        public HashSet<Food> AnimalsAndFoodAllowedInCurrentBiome
        {
            get => _animalsAndFoodAllowedInCurrentBiome;
        }

        protected Biome(
            string name,
            HashSet<Food> animalsAndFoodAllowedInCurrentBiome,
            List<KeyValuePair<int, Food>> currentAnimalsAndFoodInTheBiome,
            KeyValuePair<int, int> position
            )
        {
            _name = name;
            _animalsAndFoodAllowedInCurrentBiome = animalsAndFoodAllowedInCurrentBiome;
            _currentAnimalsAndFoodInTheBiome = currentAnimalsAndFoodInTheBiome;
            _position = position;
        }

        private bool canAnimalBeMovedInThisBiome(Animal obj)
        {
            if (_animalsAndFoodAllowedInCurrentBiome.Contains(obj))
            {
                return true;
            }
            return false;
        }

        private int findAnimalOrFoodPosInList(Food obj)
        {
            for (int i = 0; i < _currentAnimalsAndFoodInTheBiome.Count; i++)
            {
                if (obj == _currentAnimalsAndFoodInTheBiome[i].Value)
                {
                    return i;
                }
            }
            return -1;
        }

        private void updateKyeOnList(KeyValuePair<int, Food> pair, int newKey)
        {
            var updatedPair = new KeyValuePair<int, Food>(newKey, pair.Value);
            pair = updatedPair;
        }

        protected void addAnimalToBiome(ref Animal obj)
        {
            int positionOfObjOnList = findAnimalOrFoodPosInList(obj);

            if (positionOfObjOnList >= 0)
            {
                //potential problem here
                updateKyeOnList(_currentAnimalsAndFoodInTheBiome[positionOfObjOnList],
                    _currentAnimalsAndFoodInTheBiome[positionOfObjOnList].Key + 1);
            }
        }

        protected void removeAnimalFromBiome(ref Animal obj)
        {
            int positionOfObjOnList = findAnimalOrFoodPosInList(obj);

            if (positionOfObjOnList >= 0 &&
                _currentAnimalsAndFoodInTheBiome[positionOfObjOnList].Key > 0)
            {
                //potential problem here
                updateKyeOnList(_currentAnimalsAndFoodInTheBiome[positionOfObjOnList],
                    _currentAnimalsAndFoodInTheBiome[positionOfObjOnList].Key - 1);
            }
            else
            {
                //TODO: throw exception here
                //don't print messeges
                Console.WriteLine($"No such animal or no more animals" +
                    $" {_currentAnimalsAndFoodInTheBiome[positionOfObjOnList].GetType().Name} to be removed");
            }
        }

        public void moveAnimalThisBiomeToAnother(Animal animalToBeMoved, Biome anotherBiome)
        {
            if (anotherBiome.canAnimalBeMovedInThisBiome(animalToBeMoved))
            {
                anotherBiome.addAnimalToBiome(ref animalToBeMoved);
                this.removeAnimalFromBiome(ref animalToBeMoved);
            }
        }

        public override bool Equals(object? obj)
        {
            return obj is Biome biome &&
                   _name == biome._name &&
                   EqualityComparer<HashSet<Food>>.Default.Equals(_animalsAndFoodAllowedInCurrentBiome,
                   biome._animalsAndFoodAllowedInCurrentBiome) &&
                   EqualityComparer<List<KeyValuePair<int, Food>>>.Default.Equals(_currentAnimalsAndFoodInTheBiome,
                   biome._currentAnimalsAndFoodInTheBiome) &&
                   EqualityComparer<KeyValuePair<int, int>>.Default.Equals(_position, biome._position);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_name, _animalsAndFoodAllowedInCurrentBiome,
                _currentAnimalsAndFoodInTheBiome, _position);
        }
    }
}


