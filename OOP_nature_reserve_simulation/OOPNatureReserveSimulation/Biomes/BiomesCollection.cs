﻿using OOPNatureReserveSimulation.Animals;
using OOPNatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPNatureReserveSimulation.Biomes
{
    public static class BiomesCollection
    {
        static public HashSet<Biome> varietyOfBiomesCollection = new HashSet<Biome>()
        {
            //TODO: fill with biomes
        };

        public static void addBiomeInCollection(Biome input)
        {
            if (!varietyOfBiomesCollection.Contains(input))
            {
                varietyOfBiomesCollection.Add(input);
            }
        }

        public static Biome getRandomFood()
        {
            Random randomizer = new Random();
            Biome[] asArray = varietyOfBiomesCollection.ToArray();
            Biome randomBiomeCollection = asArray[randomizer.Next(asArray.Length)];
            return randomBiomeCollection;
        }
    }
}
