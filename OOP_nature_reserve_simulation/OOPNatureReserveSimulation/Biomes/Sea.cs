﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.Animals;

namespace OOPNatureReserveSimulation.Biomes
{
    public class Sea : Biome
    {
        //TODO: hardcode the number of currentAnimals in the Biome
        public Sea(List<KeyValuePair<int, Food>> currentAnimalsInTheBiome, KeyValuePair<int, int> pos)
            : base("Sea", new HashSet<Food>() { new Fish(), new SeaEagle() },
                 currentAnimalsInTheBiome, pos)
        {
        }
    }
}
