﻿using OOPNatureReserveSimulation.Plants;

namespace OOPNatureReserveSimulation.Foods
{
    class Blueberries : Plant
    {
        public Blueberries()
            : base((int)FoodProductsNutritionalVal.e_blueberries,"Blueberries")
        { }
    }

}

