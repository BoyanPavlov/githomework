﻿using OOPNatureReserveSimulation.Plants;

namespace OOPNatureReserveSimulation.Foods
{
    class Carrots : Plant
    {
        public Carrots()
               : base((int)FoodProductsNutritionalVal.e_carrots, "Carrots")
        { }
    }
}
