﻿
namespace OOPNatureReserveSimulation.Foods
{
    public enum AnimalsNutrVal : int
    {
        None = 0,
        e_seaegle = 3,
        e_cat = 4,
        e_dog = 4,
        e_wolf = 5,
        e_cow = 7
    };

    public enum FoodProductsNutritionalVal : int
    {
        e_blueberries = 7,
        e_carrots = 7,
        e_grass = 7,
        e_strawberries = 7,
        e_fish = 4,
        e_meat = 4,
        e_honey = 3,
        e_milk = 3,
        e_nuts = 3,
        e_bones = 3,
    }
    public abstract class Food
    {
        protected int _nutritionValue;
        protected string _name;
        public int NutritionValue { get => _nutritionValue; }
        public string Name
        {
            get;
            protected set;
        }

        protected Food(int nutritionValue, string name)
        {
            _nutritionValue = nutritionValue;
            _name = name;
        }

        public virtual bool isPlant()
        {
            return false;
        }

        public virtual bool isAnimal()
        {
            return false;
        }

        public virtual void regenerate()
        {
            //implementation on plants
            // we need this only for plants
        }

        public override bool Equals(object? obj)
        {
            return obj is Food food &&
                   _name == food._name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_name);
        }
    }
}
