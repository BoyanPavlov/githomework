﻿
using OOPNatureReserveSimulation.Foods;
using System.Collections.Generic;
using OOPNatureReserveSimulation.Animals;

namespace OOPNatureReserveSimulation.Foods
{
    public static class FoodCollection
    {
        static public HashSet<Food> varietyOfFoodCollection = new HashSet<Food>()
        {
            new Blueberries(), new Carrots(),new Strawberries(),new Grass(),
            new Honey(),new Nuts (), new Milk(),new Bones(),
            new Meat() ,  new Fish (), new Cow(), new SeaEagle()
        };


        //catsDiet = "Meat", "Fish", "Milk", "seaeagle" 
        //dogsDiet = "Bones", "Meat"
        //wolfsDiet = "Bones", "Meat", "Cow
        //cowsDiet = "Blueberries", "Carrots", "Strawberries", "Grass"
        //seaeaglesDiet = "Blueberries", "Carrots", "Strawberries", "Grass","Meat" ,"Fish"

        public static void addFoodInCollection(Food input)
        {
            if (!varietyOfFoodCollection.Contains(input))
            {
                varietyOfFoodCollection.Add(input);
            }
        }

        public static Food getRandomFood()
        {
            Random randomizer = new Random();
            Food[] asArray = varietyOfFoodCollection.ToArray();
            Food randomFoodCollection = asArray[randomizer.Next(asArray.Length)];
            return randomFoodCollection;
        }

    }
}