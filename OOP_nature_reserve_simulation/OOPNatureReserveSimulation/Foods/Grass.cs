﻿using OOPNatureReserveSimulation.Plants;

namespace OOPNatureReserveSimulation.Foods
{
    class Grass : Plant
    {
        public Grass()
               : base((int)FoodProductsNutritionalVal.e_grass, "Grass")
        { }
    }

}
