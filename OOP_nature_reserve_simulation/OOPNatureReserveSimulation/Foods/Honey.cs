﻿using OOPNatureReserveSimulation.Foods;

namespace OOPNatureReserveSimulation.Foods
{
    class Honey : Food
    {
        public Honey()
               : base((int)FoodProductsNutritionalVal.e_honey, "Honey")
        { }
    }
}
