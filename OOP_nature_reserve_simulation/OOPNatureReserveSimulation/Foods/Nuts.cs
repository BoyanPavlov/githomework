﻿namespace OOPNatureReserveSimulation.Foods
{
    class Nuts : Food
    {
        public Nuts()
               : base((int)FoodProductsNutritionalVal.e_nuts, "Nuts")
        { }
    }
}
