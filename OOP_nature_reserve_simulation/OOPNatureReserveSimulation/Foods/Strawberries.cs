﻿using OOPNatureReserveSimulation.Plants;

namespace OOPNatureReserveSimulation.Foods
{    class Strawberries : Plant
    {
        public Strawberries()
               : base((int)FoodProductsNutritionalVal.e_strawberries, "Strawberries")
        { }
    }
}
