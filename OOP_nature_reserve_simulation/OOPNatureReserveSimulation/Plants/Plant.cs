﻿using OOPNatureReserveSimulation.Foods;

namespace OOPNatureReserveSimulation.Plants
{
    public class Plant : Food
    {
        private int _maxNutritionValue;

        public Plant(int maxNutritionValue, string name)
            : base(maxNutritionValue, name)
        {
            _maxNutritionValue = maxNutritionValue;
        }

        public void decreaseNutritionValueWhenEaten()
        {
            _nutritionValue--;
            if (_nutritionValue < 1)
            {
                _nutritionValue = 1;
            }
        }
        public override sealed bool isPlant()
        {
                return true;
        }

        public override sealed void regenerate()
        {
            _nutritionValue = (_maxNutritionValue * 2) - (_maxNutritionValue / 2);
            _maxNutritionValue = _nutritionValue + 1;
        }

    }
}
