﻿namespace OOPNatureReserveSimulation.UI
{
    public class Behavior
    {
        private string _type;
        private string _feedingSound;
        private string _dyingSound;
        private string _hungrySound;
        public Behavior(string type, string feedingSound, string dyingSound, string hungrySound)
        {
            _type = type;
            _feedingSound = feedingSound;
            _dyingSound = dyingSound;
            _hungrySound = hungrySound;
        }
        public void FeedingSound()
        {
            Console.WriteLine($"{_type} is eating, {_feedingSound}");
        }
        public void DyingSound()
        {
            Console.WriteLine($"{_type} is dead, {_dyingSound}");
        }
        public void HungrySound()
        {
            Console.WriteLine($"{_type} is hungry, {_hungrySound}");
        }
        public void StarvingSound()
        {
            Console.WriteLine($"{_type} is starving");
        }

        public void errorMessageForFeeding()
        {
            Console.WriteLine("It's dead, stop feeding it");
        }

        public void animalHasBeenEatenMessege()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (int i = 0; i < 10; i++)
            {
                Console.Write("=");
            }
                Console.Write($"{_type} has been eaten, {_dyingSound}!");
            for (int i = 0; i < 9; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine("=");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
