﻿using OOPNatureReserveSimulation.Animals;

namespace OOPNatureReserveSimulation.UI
{
    public class InterfaceFunctinality
    {
        protected void printLargerInterface(ref HashSet<Animal> _collectionOfAnimals)
        {
            string alive = "Alive";
            string dead = "Dead";
            string line = "======================";

            Console.WriteLine(line);

            foreach (var animal in _collectionOfAnimals)
            {
                Console.Write(animal.GetType().Name);
                int putSpaces = 17 - animal.GetType().Name.Length;
                for (int i = 0; i < putSpaces; i++)
                {
                    Console.Write(" ");
                }

                if (animal.isAlive)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(alive);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(dead);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            Console.WriteLine(line);
        }

        protected void printFinalStatistics(ref List<Animal> _collectionOfAnimals)
        {

            Animal minLifeSpan = _collectionOfAnimals[0];
            Animal longestLifeSpan = _collectionOfAnimals[_collectionOfAnimals.Count - 1];

            int middleIndex = (_collectionOfAnimals.Count - 1) / 2;
            Animal averageLifeSpan = _collectionOfAnimals[middleIndex];

            printSentence("shortest", minLifeSpan.GetType().Name, minLifeSpan.LifeSpan);
            printSentence("average", averageLifeSpan.GetType().Name, averageLifeSpan.LifeSpan);
            printSentence("longest", longestLifeSpan.GetType().Name, longestLifeSpan.LifeSpan);
        }

        private void printSentence(string variationOfTime, string animalName, int days)
        {
            Console.WriteLine($"Animal with {variationOfTime} lifespan of: {animalName} with {days} days");
        }

    }
}
