﻿using OOPNatureReserveSimulation.Animals;

namespace OOPNatureReserveSimulation.UI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            char choice;
            do
            {
                Console.WriteLine("\nDo you want larger interface?\n");
                Console.WriteLine("Press \'y\' if you want.\nPress \'n\' if you don't");
                choice = Console.ReadKey().KeyChar;

            } while (choice != 'y' && choice != 'n');

            bool doWeStartWithLargeInterface = (choice == 'y') ? true : false;

            SimulationLifespan simulate = new SimulationLifespan();

            simulate.startSimulation(doWeStartWithLargeInterface);
        }
    }
}