﻿using OOPNatureReserveSimulation.Animals;
using OOPNatureReserveSimulation.Foods;
using OOPNatureReserveSimulation.Plants;

namespace OOPNatureReserveSimulation.UI
{
    public class SimulationLifespan : InterfaceFunctinality
    {
        private int day = 0;
        private HashSet<Animal> _collectionOfAnimals;
        private Food randomFood = FoodCollection.getRandomFood();

        public SimulationLifespan()
        {
            _collectionOfAnimals = AnimalCollection.varietyOfAnimals;
        }

        private bool allAnimalsAreDead()
        {
            foreach (var item in _collectionOfAnimals)
            {
                if (item.isAlive)
                {
                    return false;
                }
            }
            return true;
        }

        private int animalsComparator(Animal animal1, Animal animal2)
        {
            return animal1.LifeSpan.CompareTo(animal2.LifeSpan);
        }

        private void regeneratingPlantsLogic(ref int day, ref Food food)
        {
            if (day % 100 > 50)
            {
                if (food.isPlant())
                {
                    food.regenerate();
                }
            }
        }

        private void animalEatingAnimalLogic(ref Food food, Animal currentAnimal)
        {
            if (currentAnimal.Diet.Contains(food) && food.isAnimal())
            {
                foreach (var animal in _collectionOfAnimals)
                {
                    if (animal.Equals(food) &&
                        animal.CurrentEnergy <= animal.MaxEnergy / 2 &&
                            currentAnimal.CurrentEnergy < currentAnimal.MaxEnergy)
                    {
                        animal.AnimalHasBeenEaten();
                    }
                }
            }
        }

        public void startSimulation(bool shouldIPrintLargerStatistics)
        {
            while (!allAnimalsAreDead())
            {
                Console.WriteLine($"\nDay: {day}");

                randomFood = FoodCollection.getRandomFood();

                regeneratingPlantsLogic(ref day, ref randomFood);

                foreach (var animal in _collectionOfAnimals)
                {
                    if (animal.isAlive)
                    {
                        animal.Feed(randomFood);

                        animalEatingAnimalLogic(ref randomFood, animal);

                        animal.LifeSpan++;
                    }
                }

                day++;

                if (shouldIPrintLargerStatistics)
                {
                    printLargerInterface(ref _collectionOfAnimals);
                }
            }

            List<Animal> ListOfAnimals = _collectionOfAnimals.ToList();
            ListOfAnimals.Sort(animalsComparator);
            printFinalStatistics(ref ListOfAnimals);
        }
    }
}
